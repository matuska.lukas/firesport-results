module.exports = {
  // location
  protocol: 'http',
  url: 'localhost',
  port: 3000,

  // full url
  fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),

  // database credentials
  db: {
    port: 27017,
    host: 'localhost',
    name: 'firesport-results',
    user: 'firesport-results',
    password: 'password'
  },

  nodemailer: {
    sender: 'Hasičské výsledky <firesport-results@example.com>',
    settings: {
      host: 'smtp.example.com',
      port: 465,
      secure: true,
      auth: {
        user: 'firesport-results@example.com',
        pass: 'random-string-for-password:)'
      },
      tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
      }
    }
  },

  redis: {
    url: 'redis://localhost:6379'
  }
}
