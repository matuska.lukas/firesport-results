/**
 * Category controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Models
 */
const Category = require('../models/Category')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.placementPoints === undefined) {
    return res.send('not-send-placement-points')
  }
  new Category({
    name: req.body.name,
    placementPoints: req.body.placementPoints,
    author: req.session.user._id
  }).save((err) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    res.send('ok')
  })
}

module.exports.list = (req, res) => {
  Category
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .exec((err, categories) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send(categories)
    })
}

module.exports.delete = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Category
    .findByIdAndDelete(req.body.id)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send('ok')
    })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  } else if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.placementPoints === undefined) {
    return res.send('not-send-placement-points')
  }
  Category
    .findByIdAndUpdate(req.body.id, {
      name: req.body.name,
      placementPoints: req.body.placementPoints
    })
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send('ok')
    })
}
