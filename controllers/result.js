/**
 * Results controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Models
 */
const Result = require('../models/Result')
// const Team = require('../models/Team')
// const User = require('../models/User')

module.exports.new = (req, res) => {
  const result = {
    author: req.session.user._id,
    date: moment(),
    result: 0,
    targets: {}
  }

  // Team
  if (req.body.team === undefined) {
    return res.send('not-send-team')
  } else {
    result.team = req.body.team
  }

  // Category
  if (req.body.category === undefined) {
    return res.send('not-send-category')
  } else {
    result.category = req.body.category
  }

  // Competition
  if (req.body.competition === undefined) {
    return res.send('not-send-competition')
  } else {
    result.competition = req.body.competition
  }

  /**
   * Targets
   */
  if (req.body.targets === undefined) {
    // Targets sent separately
    // Left target
    if (req.body.left === undefined) {
      return res.send('not-send-left-target')
    } else if (req.body.left < 0) {
      return res.send('invalid-range-left-target')
    } else {
      result.targets.left = req.body.left
    }

    // Right target
    if (req.body.right === undefined) {
      return res.send('not-send-right-target')
    } else if (req.body.right < 0) {
      return res.send('invalid-range-right-target')
    } else {
      result.targets.right = req.body.right
    }
  }

  // Targets sent as object
  // Left target
  if (req.body.targets.left === undefined) {
    return res.send('not-send-left-target-obj')
  } else if (req.body.targets.left !== undefined) {
    result.targets.left = req.body.targets.left
  }
  // Right target
  if (req.body.targets.right === undefined) {
    return res.send('not-send-right-target-obj')
  } else if (req.body.targets.right !== undefined) {
    result.targets.right = req.body.targets.right
  }

  // Final time
  if (req.body.finalTime !== undefined) {
    if (req.body.finalTime !== 0) {
      result.result = (result.targets.left >= result.targets.right ? result.targets.left : result.targets.right)
    }
  }

  if (req.body.media !== undefined) {
    if (req.body.media.youtube !== undefined) {
      result.media.youtube = req.body.media.youtube.trim()
    }
  }

  // New record to database
  new Result(result).save((err) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    return res.send('ok')
  })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Result
    .findById(req.body.id)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .populate({
      path: 'team',
      select: 'name district',
      populate: 'district'
    })
    .populate('category')
    .populate('competition')
    .exec((err, update) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }

      // Team
      if (req.body.team !== undefined) {
        update.team = req.body.team
      }

      // Category
      if (req.body.category !== undefined) {
        update.category = req.body.category
      }

      // Competition
      if (req.body.competition !== undefined) {
        update.competition = req.body.competition
      }

      if (req.body.media !== undefined) {
        if (req.body.media.youtube !== undefined) {
          update.media.youtube = req.body.media.youtube.trim()
        }
      }

      /**
       * Targets
       */
      if (req.body.targets === undefined) {
        // Targets sent separately
        // Left target
        if (req.body.left !== undefined) {
          if (req.body.left >= 0) {
            update.targets.left = req.body.left
          }
        }

        // Right target
        if (req.body.right !== undefined) {
          if (req.body.right >= 0) {
            update.targets.right = req.body.right
          }
        }
      }

      // Targets sent as object
      // Left target
      if (req.body.targets.left !== undefined) {
        update.targets.left = req.body.targets.left
      }
      // Right target
      if (req.body.targets.right !== undefined) {
        update.targets.right = req.body.targets.right
      }

      // Final time
      if (req.body.finalTime !== undefined) {
        if (req.body.finalTime !== 0) {
          update.result = (update.targets.left >= update.targets.right ? update.targets.left : update.targets.right)
        }
      }

      // New record to database
      Result
        .findByIdAndUpdate(req.body.id, update)
        .exec((err) => {
          if (err) {
            res.send('err')
            return console.error(err)
          }
          return res.send('ok')
        })
    })
}

module.exports.list = (req, res) => {
  Result
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .populate({
      path: 'team',
      select: 'name district',
      populate: 'district'
    })
    .populate('category')
    .populate('competition')
    .sort([['result', 1]])
    .exec((err, results) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      if (results.length !== 0) {
        while (results[0].result === 0) {
          results.push(results[0])
          results.shift()
        }
      }
      res.send(results)
    })
}

module.exports.findById = (req, res) => {
  if (req.params.id === undefined) {
    return res.send('not-send-id')
  }
  Result
    .findById(req.params.id)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .populate({
      path: 'team',
      select: 'name district',
      populate: 'district'
    })
    .populate('category')
    .populate('competition')
    .exec((err, result) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send(result)
    })
}

module.exports.delete = (req, res) => {
  const id = req.body.id !== undefined ? req.body.id : req.query.id
  if (id === null) {
    return res.send('not-send-id')
  }

  Result.findByIdAndDelete(id, (err, result) => {
    if (err) {
      res.send(err)
      return console.error(err)
    }
    return res.send('ok')
  })
}
