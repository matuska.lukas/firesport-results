/**
 * Admin page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const User = require('../../models/User')
const League = require('../../models/League')
const Competition = require('../../models/Competition')
const Category = require('../../models/Category')
const Result = require('../../models/Result')
const Team = require('../../models/Team')
const District = require('../../models/District')

module.exports.dashboard = (req, res) => {
  res.render('admin/dashboard', { req, res, active: 'dashboard', title: 'Přehled' })
}

module.exports.leagues = {
  list: (req, res) => {
    res.render('admin/leagues/list', { req, res, active: 'leagues', title: 'Seznam lig' })
  },
  new: (req, res) => {
    res.render('admin/leagues/new', { req, res, active: 'leagues', title: 'Nová liga' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Liga nenalezena', 'Ups! Hledáte ligu, která není v databázi')
    }
    League
      .findById(req.params.id)
      .exec((err, league) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        } else if (league === null) {
          return this.error.notFound(req, res, 'Liga nenalezena', 'Ups! Hledáte ligu, která není v databázi')
        }
        res.render('admin/leagues/edit', { req, res, active: 'leagues', title: 'Editace ligy', league })
      })
  }
}

module.exports.competitions = {
  list: (req, res) => {
    res.render('admin/competitions/list', { req, res, active: 'competitions', title: 'Seznam soutěží' })
  },
  new: (req, res) => {
    res.render('admin/competitions/new', { req, res, active: 'competitions', title: 'Nová soutěž' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Soutěž nenalezena', 'Ups! Hledáte soutěž, která není v databázi')
    }
    res.render('admin/competitions/edit', { req, res, active: 'competitions', title: 'Editace soutěže' })
  }
}

module.exports.categories = {
  list: (req, res) => {
    res.render('admin/categories/list', { req, res, active: 'categories', title: 'Seznam kategorií' })
  },
  new: (req, res) => {
    res.render('admin/categories/new', { req, res, active: 'categories', title: 'Nová kategorie' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Kategorie nenalezena', 'Ups! Hledáte kategorii, která není v databázi')
    }
    Category
      .findById(req.params.id)
      .exec((err, category) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        } else if (category === null) {
          return this.error.notFound(req, res, 'Kategorie nenalezena', 'Ups! Hledáte kategorii, která není v databázi')
        }
        res.render('admin/categories/edit', { req, res, active: 'categories', title: 'Editace kategorie', category })
      })
  }
}

module.exports.teams = {
  list: (req, res) => {
    res.render('admin/teams/list', { req, res, active: 'teams', title: 'Seznam týmů' })
  },
  new: (req, res) => {
    res.render('admin/teams/new', { req, res, active: 'teams', title: 'Nový tým' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Tým nenalezen', 'Ups! Hledáte tým, který není v databázi')
    }
    Team
      .findById(req.params.id)
      .exec((err, team) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        } else if (team === null) {
          return this.error.notFound(req, res, 'Tým nenalezen', 'Ups! Hledáte tým, který není v databázi')
        }
        res.render('admin/teams/edit', { req, res, active: 'teams', title: 'Editace týmu', team })
      })
  }
}

module.exports.districts = {
  list: (req, res) => {
    res.render('admin/districts/list', { req, res, active: 'districts', title: 'Seznam okresů' })
  },
  new: (req, res) => {
    res.render('admin/districts/new', { req, res, active: 'districts', title: 'Nový okres' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Okres nenalezen', 'Ups! Hledáte okres, který není v databázi')
    }
    District
      .findById(req.params.id)
      .exec((err, district) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        } else if (district === null) {
          return this.error.notFound(req, res, 'Okres nenalezen', 'Ups! Hledáte okres, který není v databázi')
        }
        res.render('admin/districts/edit', { req, res, active: 'districts', title: 'Editace okresu', district })
      })
  }
}

module.exports.results = {
  list: (req, res) => {
    res.render('admin/results/list', { req, res, active: 'results', title: 'Seznam výsledků' })
  },
  new: (req, res) => {
    res.render('admin/results/new', { req, res, active: 'results', title: 'Nový výsledek' })
  },
  edit: (req, res) => {
    if (req.params.id === undefined) {
      return this.error.notFound(req, res, 'Výsledek nenalezen', 'Ups! Hledáte výsledek, který není v databázi')
    }
    Result
      .findById(req.params.id)
      .exec((err, result) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        } else if (result === null) {
          return this.error.notFound(req, res, 'Výsledek nenalezen', 'Ups! Hledáte výsledek, který není v databázi')
        }
        res.render('admin/results/edit', { req, res, active: 'results', title: 'Editace výsledku', result })
      })
  }
}

module.exports.users = {
  list: (req, res) => {
    res.render('admin/users/list', { req, res, active: 'users', title: 'Seznam uživatelů' })
  },
  new: (req, res) => {
    res.render('admin/users/new', { req, res, active: 'users', title: 'Nový uživatel' })
  },
  edit: (req, res) => {
    User
      .findById(req.params.id)
      .exec((err, user) => {
        if (err) {
          this.error.internalError(req, res)
          return console.error(err)
        }
        if (user === null) {
          return this.error.notFound(
            req, res,
            '404 Uživatel nenalezen',
            'Hledáte uživatele, který se nenachází v databázi, přeji Vám příjmenou hru na schovávanou.'
          )
        }
        res.render('admin/users/edit', { req, res, active: 'users', title: 'Editace uživatele', user })
      })
  }
}

module.exports.profile = (req, res) => {
  res.render('admin/profile', { req, res, active: 'profile', title: 'Profil' })
}

module.exports.error = {
  accessDenied: (req, res) => {
    res.render('admin/errors/universal', {
      req, res, active: 'error', title: '403 Přístup odepřen', bgClass: 'bg-access-denied', description: `Chcete se dostat někam, kde nemáte udělený přístup, pokud si myslíte, že na něj máte
                nárok, zažádejte si o něj u správce.`
    })
  },
  notFound: (req, res, title = '404 Nenalezeno', description = 'Hledáte soubor, který se tu nenachází, přeji Vám příjmenou hru na schovávanou.') => {
    res.render('admin/errors/universal', { req, res, active: 'error', title, description, bgClass: 'bg-not-found' })
  },
  internalError: (req, res) => {
    res.render('admin/errors/internal', { req, res, active: 'error', title: '500 Chyba serveru' })
  }
}
