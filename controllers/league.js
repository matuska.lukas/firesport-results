/**
 * League controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Models
 */
const League = require('../models/League')

module.exports.new = (req, res) => {
  /**
   * Method for new League
   */
  // Left target
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  }

  // New record to database
  new League({
    name: req.body.name.trim(),
    author: req.session.user._id
  }).save((err) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    return res.send('ok')
  })
}

module.exports.list = (req, res) => {
  League
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, results) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send(results)
    })
}

module.exports.delete = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }

  League
    .findByIdAndDelete(req.body.id)
    .exec((err) => {
      if (err) {
        res.send(err)
        return console.error(err)
      }
      return res.send('ok')
    })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  } else if (req.body.name === undefined) {
    return res.send('not-send-name')
  }
  League
    .findByIdAndUpdate(req.body.id, {
      name: req.body.name.trim()
    })
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send('ok')
    })
}

module.exports.setLeagueSession = (req, callback) => {
  if (req.params.leagueId !== undefined) {
    League
      .findById(req.params.leagueId)
      .populate({
        path: 'author',
        select: 'name photo email'
      })
      .exec((err, league) => {
        if (err) {
          callback('err')
          return console.error(err)
        }
        req.session.league = league
        callback('ok')
      })
  }
}
