/**
 * Competitions controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Models
 */
const Competition = require('../models/Competition')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.date === undefined) {
    return res.send('not-send-date')
  } else if (req.body.league === undefined) {
    return res.send('not-send-league')
  } else if (req.body.categories === undefined) {
    return res.send('not-send-categories')
  }
  new Competition({
    name: req.body.name,
    date: moment(req.body.date),
    league: req.body.league,
    categories: req.body.categories,
    author: req.session.user.__id
  }).save((err, competition) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    res.send('ok')
  })
}

module.exports.list = (req, res) => {
  Competition
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .populate('league')
    .populate('categories')
    .exec((err, competitions) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send(competitions)
    })
}

module.exports.findById = (req, res) => {
  if (req.params.id === undefined) {
    return res.send('not-send-id')
  }
  Competition
    .findById(req.params.id)
    .populate('league')
    .populate('categories')
    .exec((err, competition) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send(competition)
    })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  } else if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.date === undefined) {
    return res.send('not-send-date')
  } else if (req.body.league === undefined) {
    return res.send('not-send-league')
  } else if (req.body.categories === undefined) {
    return res.send('not-send-categories')
  }
  Competition
    .findByIdAndUpdate(req.body.id, {
      name: req.body.name,
      date: req.body.date,
      league: req.body.league,
      categories: req.body.categories
    })
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send('ok')
    })
}

module.exports.delete = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Competition
    .findByIdAndDelete(req.body.id)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send('ok')
    })
}
