/**
 * Team controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Team = require('../models/Team')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.categories === undefined) {
    return res.send('not-send-categories')
  } else if (req.body.district === undefined) {
    return res.send('not-send-district')
  }
  new Team({
    name: req.body.name.trim(),
    categories: req.body.categories,
    district: req.body.district,
    author: req.session.user._id
  }).save((err, team) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    res.send('ok')
  })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  const update = {}
  if (req.body.name !== undefined) {
    update.name = req.body.name
  }
  if (req.body.categories !== undefined) {
    update.categories = req.body.categories
  }
  if (req.body.district !== undefined) {
    update.district = req.body.district
  }
  Team
    .findByIdAndUpdate(req.body.id, update)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send('ok')
    })
}

module.exports.delete = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  Team
    .findByIdAndDelete(req.body.id)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send('ok')
    })
}

module.exports.list = (req, res) => {
  Team
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .populate('categories')
    .populate('district')
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, teams) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send(teams)
    })
}

module.exports.findById = (req, res) => {
  Team
    .findById(req.params.id)
    .populate('categories')
    .populate('district')
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, team) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send(team)
    })
}

module.exports.findByName = (req, res) => {
  if (req.query.name === undefined) {
    return res.send('not-send-name')
  }
  Team
    .findOne(
      {
        name: new RegExp(req.query.name, 'i')
      }
    )
    .populate('categories')
    .populate('district')
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, team) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      res.send(team)
    })
}
