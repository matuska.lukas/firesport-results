/**
 * District controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const District = require('../models/District')

module.exports.new = (req, res) => {
  if (req.body.name === undefined) {
    return res.send('not-send-name')
  } else if (req.body.shortname === undefined) {
    return res.send('not-send-shortname')
  }
  new District({
    name: {
      full: req.body.name.trim(),
      short: req.body.shortname.trim()
    },
    author: req.session.user._id
  }).save((err, district) => {
    if (err) {
      res.send('err')
      return console.error(err)
    }
    return res.send('ok')
  })
}

module.exports.edit = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  const update = {
    name: {}
  }
  if (req.body.name !== undefined) {
    update.name.full = req.body.name
  }
  if (req.body.shortname !== undefined) {
    update.name.short = req.body.shortname
  }
  if (Object.keys(update.name).length === 0) {
    update.name = undefined
  }
  District
    .findByIdAndUpdate(req.body.id, update)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send('ok')
    })
}

module.exports.delete = (req, res) => {
  if (req.body.id === undefined) {
    return res.send('not-send-id')
  }
  District
    .findByIdAndDelete(req.body.id)
    .exec((err) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send('ok')
    })
}

module.exports.list = (req, res) => {
  District
    .find(req.query.filter === undefined ? {} : req.query.filter)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, districts) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send(districts)
    })
}

module.exports.findById = (req, res) => {
  if (req.params.id === undefined) {
    return res.send('not-send-id')
  }
  District
    .findById(req.params.id)
    .populate({
      path: 'author',
      select: 'name photo email'
    })
    .exec((err, districts) => {
      if (err) {
        res.send('err')
        return console.error(err)
      }
      return res.send(districts)
    })
}
