/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

module.exports.homepage = (req, res) => {
  res.render('homepage', { req, res, title: '', active: 'homepage' })
}

module.exports.leagues = (req, res) => {
  res.render('leagues', { req, res, title: 'Seznam lig', active: 'leagues' })
}

module.exports.competitions = (req, res) => {
  res.render('competitions', { req, res, title: 'Seznam soutěží', active: 'competitions' })
}

module.exports.contact = (req, res) => {
  res.render('contact', {req, res, title: 'Kontakt', active: 'contact' })
}

module.exports.login = (req, res) => {
  res.render('login/login', { req, res, title: 'Přihlášení', active: 'login' })
}

module.exports.register = (req, res) => {
  res.render('login/register', { req, res, title: 'Registrace', active: 'register' })
}

module.exports.results = (req, res) => {
  res.render('results', { req, res, title: '', active: 'results' })
}

module.exports.error = {
  accessDenied: (req, res, title = '403 Přístup odepřen', description = 'Jejda! Snažíte se dostat někam, kam očividně nemáte právo.') => {
    res.render('errors/universal', { req, res, active: 'error', title, description })
  },
  notFound: (req, res, title = '404 Nenalezeno', description = 'Hledáte soubor, který se tu nenachází, přeji Vám příjmenou hru na schovávanou.') => {
    res.render('errors/universal', { req, res, active: 'error', title, description })
  },
  internalError: (req, res, error = '') => {
    res.render('errors/internal', { req, res, active: 'error', title: '500 Chyba serveru', error })
  }
}
