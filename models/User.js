/**
 * User database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var userSchema = new mongoose.Schema({
  name: {
    first: String,
    last: String
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  rescue: {
    enabled: Boolean,
    default: false
  },
  type: {
    type: String,
    enum: ['admin', 'editor', 'guest'],
    default: 'guest'
  },
  photo: {
    type: String,
    default: '/images/users/_default.png'
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  logins: [{
    date: Date,
    ip: String
  }]
})

// export
module.exports = mongoose.model('User', userSchema, 'user')
