/**
 * Competition database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var competitionSchema = new mongoose.Schema({
  name: String,
  date: Date,
  league: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'League'
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }]
})

// export
module.exports = mongoose.model('Competition', competitionSchema, 'competition')
