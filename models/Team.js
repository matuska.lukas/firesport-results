/**
 * Team database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var teamSchema = new mongoose.Schema({
  name: String,
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  district: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'District'
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
})

// export
module.exports = mongoose.model('Team', teamSchema, 'team')
