/**
 * Result database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var resultSchema = new mongoose.Schema({
  competition: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Competition',
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  targets: {
    left: {
      type: Number,
      required: true
    },
    right: {
      type: Number,
      required: true
    }
  },
  result: {
    type: Number,
    required: true
  },
  media: {
    youtube: {
      type: String,
      default: '#'
    }
  },
  date: {
    type: Date,
    required: true
  }
})

// export
module.exports = mongoose.model('Result', resultSchema, 'result')
