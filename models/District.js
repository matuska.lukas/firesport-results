/**
 * District database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../libs/db')

// the schema itself
var districtSchema = new mongoose.Schema({
  name: {
    full: String,
    short: String
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
})

// export
module.exports = mongoose.model('District', districtSchema, 'district')
