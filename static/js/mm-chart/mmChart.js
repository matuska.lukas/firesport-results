class Graph {

    constructor(element, negative, max, spacing, grid = true, styling = ["stroke:rgb(255,0,0);stroke-width:2", "transparent", "stroke:black;stroke-width:1", "stroke:grey;stroke-width:1"]) {
        this.element = element;
        this.width = element.width.baseVal.value;
        this.height = element.height.baseVal.value;
        this.max = max;
        this.spacing = spacing;
        this.record = [];
        this.negative = negative;
        this.grid = grid;
        this.styling = styling;
    }

    renderGrid() {
        var i = 0;
        var result = "";
        this.element.style.background = this.styling[1];
        var current;
        while (i < (this.width / this.spacing)) {
            // result += "<line x1=\"" + (i * this.spacing) + "\" y1=\"0\" x2=\"" + (i * this.spacing) + "\" y2=\"" + (this.max * this.height) + "\" style=\"" + this.styling[2] + "\" />";
            result += `<line x1="${(i * this.spacing)}" y1="0" x2="${(i * this.spacing)}" y2="${(this.max * this.height)}" style="${this.styling[2]}" />`;
            i++;
        }
        i = 0;
        while (i < (this.height / this.max * 2)) {
            result += "<line x1=\"0\" y1=\"" + (i * this.max / 2) + "\" x2=\"" + (this.width) + "\" y2=\"" + (i * this.max / 2) + "\" style=\"" + this.styling[3] + "\" />";
            i++;
        }
        return result;
    }

    render(offset = 0) {
        var result = "";
        if (this.grid) result = this.renderGrid();
        var i = offset;
        var previous;
        var current;
        while (i < this.record.length) {
            if (this.negative) current = this.max / 2 - this.record[i];
            else current = this.max - this.record[i];

            if (i != offset)
                result += "<line x1=\"" + ((i - 1) * this.spacing) + "\" y1=\"" + (previous / this.max * this.height)
                    + "\" x2=\"" + ((i - 1) * this.spacing + this.spacing) + "\" y2=\"" + (current / this.max * this.height) + "\" style=\"" + this.styling[0] + "\" />";
            previous = current;
            i++;
        }
        this.element.innerHTML = result;
    }

    add(value) {
        if (value instanceof Array) this.record = this.record.concat(value);
        else this.record.push(value);
    }

    insert(i, value) {
        this.record.splice(i, 0, value);
    }

    remove(i, value) {
        this.record.splice(i, 1);
    }

}