var API = {
  league: {
    new: function (name) {
      return $.post(
        '/api/leagues/new',
        {
          name: name
        },
        'json'
      )
    },

    list: function () {
      return $.get('/api/leagues/list', {}, 'json')
    },

    delete: function (id) {
      return $.post(
        '/api/leagues/delete',
        {
          id: id
        },
        'json'
      )
    },

    edit: function (id, name) {
      return $.post(
        '/api/leagues/edit',
        {
          id: id,
          name: name
        },
        'json'
      )
    }
  },

  category: {
    new: function (name, placementPoints) {
      return $.post(
        '/api/categories/new',
        {
          name: name,
          placementPoints: placementPoints
        },
        'json'
      )
    },

    list: function (filter = {}) {
      return $.get('/api/categories/list', {
        filter: filter
      }, 'json')
    },

    delete: function (id) {
      return $.post(
        '/api/categories/delete',
        {
          id: id
        },
        'json'
      )
    },

    edit: function (id, name, placementPoints) {
      return $.post(
        '/api/categories/edit',
        {
          id: id,
          name: name,
          placementPoints: placementPoints
        },
        'json'
      )
    }
  },

  competition: {
    new: function (name, date, leagueId, categories) {
      return $.post(
        '/api/competitions/new',
        {
          name: name,
          date: date,
          league: leagueId,
          categories: categories
        },
        'json'
      )
    },

    list: function (filter = {}) {
      return $.get('/api/competitions/list', {
        filter: filter
      }, 'json')
    },

    findById: function (id) {
      return $.get(`/api/competitions/find-by-id/${id}`, {}, 'json')
    },

    delete: function (id) {
      return $.post(
        '/api/competitions/delete',
        {
          id: id
        },
        'json'
      )
    },

    edit: function (id, name, date, leagueId, categories) {
      return $.post(
        '/api/competitions/edit',
        {
          id: id,
          name: name,
          date: date,
          league: leagueId,
          categories: categories
        },
        'json'
      )
    }
  },

  district: {
    new: function (fullname, shortname) {
      return $.post(
        '/api/districts/new',
        {
          name: fullname,
          shortname: shortname
        },
        'json'
      )
    },

    list: function (filter = {}) {
      return $.get('/api/districts/list', {
        filter: filter
      }, 'json')
    },

    findById: function (id) {
      return $.get('/api/districts/find-by-id/' + id, {}, 'json')
    },

    delete: function (id) {
      return $.post(
        '/api/districts/delete',
        {
          id: id
        },
        'json'
      )
    },

    edit: function (id, fullname, shortname) {
      return $.post(
        '/api/districts/edit',
        {
          id: id,
          name: fullname,
          shortname: shortname
        },
        'json'
      )
    }
  },

  team: {
    new: function (name, categories, district) {
      return $.post(
        '/api/teams/new',
        {
          name: name,
          categories: categories,
          district: district
        },
        'json'
      )
    },

    list: function (filter = {}) {
      return $.get('/api/teams/list', {
        filter: filter
      }, 'json')
    },

    findById: function (id) {
      return $.get('/api/teams/find-by-id/' + id, {}, 'json')
    },

    delete: function (id) {
      return $.post(
        '/api/teams/delete',
        {
          id: id
        },
        'json'
      )
    },

    edit: function (id, name, categories, district) {
      return $.post(
        '/api/teams/edit',
        {
          id: id,
          name: name,
          categories: categories,
          district: district
        },
        'json'
      )
    }
  },

  user: {
    new: function (
      email,
      password,
      firstname,
      lastname,
      middlename,
      usertype) {
      return $.post(
        '/api/user/new',
        {
          email: email,
          password: password,
          firstname: firstname,
          middlename: middlename,
          lastname: lastname,
          usertype: usertype
        },
        'json'
      )
    },

    login: function (email, password) {
      return $.post(
        '/api/user/login',
        {
          email: email,
          password: password
        },
        'json'
      )
    },

    edit: function (object) {
      return $.post(
        '/api/user/edit',
        object,
        'json'
      )
    },

    forgotPassword: function (email) {
      return $.post(
        '/api/user/forgot-password',
        {
          email: email
        },
        'json'
      )
    },

    setNewPassword: function (userId, password) {
      return $.post(
        '/api/user/set-new-password',
        {
          id: userId,
          password: password
        },
        'json'
      )
    },

    updateSession: function () {
      return $.get('/api/user/update-session', {}, 'json')
    },

    changeType: function (userId, type) {
      return $.post(
        '/api/user/change-type',
        {
          id: userId,
          type: type
        },
        'json'
      )
    },

    list: function (filter = {}) {
      return $.get('/api/user/list', {
        filter: filter
      }, 'json')
    },

    logout: function () {
      return $.get('/api/user/logout', {}, 'json')
    },

    delete: function (userId) {
      return $.post(
        '/api/user/delete',
        {
          id: userId
        },
        'json'
      )
    },

    loggedIn: function () {
      return $.get('/api/user/am-i-logged-in', {}, 'json')
    }
  },

  result: {
    new: function (requestBody) {
      return $.post(
        '/api/results/new',
        requestBody,
        'json'
      )
    },

    edit: function (requestBody) {
      return $.post(
        '/api/results/edit',
        requestBody,
        'json'
      )
    },

    findById: function (id) {
      return $.get('/api/results/find-by-id/' + id, {}, 'json')
    },

    list: function (filter = {}) {
      return $.get(
        '/api/results/list',
        {
          filter: filter
        },
        'json'
      )
    }
  }
}

function formatResult(result, $dom) {
  switch (result) {
    case 0:
      $dom.html('N')
      break

    case -1:
      $dom.html('D')
      break

    default:
      $dom.html(result)
      break
  }
}
