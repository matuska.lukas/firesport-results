/**
 * Admin router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router()

/**
 * Libraries
 */
const moment = require('moment')
moment.locale('cs')

/**
 * Controllers
 */
const adminPageController = require('../controllers/admin/page')
const pageController = require('../controllers/page')
// const partials = require('./partials')

/**
 * Routes
 */

/**
 * Make admin section a little bit secret
 */
router.get('/*', (req, res, next) => {
  if (req.session.user !== undefined) {
    if (req.session.user.type === 'admin') {
      return next()
    }
  }
  pageController.error.notFound(req, res)
})

/**
 * Dashboard
 */
router.get('/', (req, res) => {
  res.redirect('./dashboard')
})

router.get('/dashboard', (req, res) => {
  adminPageController.dashboard(req, res)
})

/**
 * Leagues
 */
router.get('/leagues/', (req, res) => {
  res.redirect('./list')
})

router.get('/leagues/list', (req, res) => {
  adminPageController.leagues.list(req, res)
})

router.get('/leagues/new', (req, res) => {
  adminPageController.leagues.new(req, res)
})

router.get('/leagues/edit/:id', (req, res) => {
  adminPageController.leagues.edit(req, res)
})

/**
 * Competitions
 */
router.get('/competitions/', (req, res) => {
  res.redirect('./list')
})

router.get('/competitions/list', (req, res) => {
  adminPageController.competitions.list(req, res)
})

router.get('/competitions/new', (req, res) => {
  adminPageController.competitions.new(req, res)
})

router.get('/competitions/edit/:id', (req, res) => {
  adminPageController.competitions.edit(req, res)
})

/**
 * Categories
 */
router.get('/categories/', (req, res) => {
  res.redirect('./list')
})

router.get('/categories/list', (req, res) => {
  adminPageController.categories.list(req, res)
})

router.get('/categories/new', (req, res) => {
  adminPageController.categories.new(req, res)
})

router.get('/categories/edit/:id', (req, res) => {
  adminPageController.categories.edit(req, res)
})

/**
 * Teams
 */
router.get('/teams/', (req, res) => {
  res.redirect('./list')
})

router.get('/teams/list', (req, res) => {
  adminPageController.teams.list(req, res)
})

router.get('/teams/new', (req, res) => {
  adminPageController.teams.new(req, res)
})

router.get('/teams/edit/:id', (req, res) => {
  adminPageController.teams.edit(req, res)
})

/**
 * Districts
 */
router.get('/districts/', (req, res) => {
  res.redirect('./list')
})

router.get('/districts/list', (req, res) => {
  adminPageController.districts.list(req, res)
})

router.get('/districts/new', (req, res) => {
  adminPageController.districts.new(req, res)
})

router.get('/districts/edit/:id', (req, res) => {
  adminPageController.districts.edit(req, res)
})

/**
 * Results
 */
router.get('/results/', (req, res) => {
  res.redirect('./list')
})

router.get('/results/list', (req, res) => {
  adminPageController.results.list(req, res)
})

router.get('/results/new', (req, res) => {
  adminPageController.results.new(req, res)
})

router.get('/results/edit/:id', (req, res) => {
  adminPageController.results.edit(req, res)
})

/**
 * Users
 */
router.get('/users/', (req, res) => {
  res.redirect('./list')
})

router.get('/users/list', (req, res) => {
  adminPageController.users.list(req, res)
})

router.get('/users/new', (req, res) => {
  adminPageController.users.new(req, res)
})

router.get('/users/edit/:id', (req, res) => {
  adminPageController.users.edit(req, res)
})

router.get('/users/detail/:id', (req, res) => {
  adminPageController.users.detail(req, res)
})

/**
 * Profile
 */
router.get('/profile', (req, res) => {
  adminPageController.profile(req, res)
})

/**
 * Error pages for test
 */
router.get('/403', (req, res) => {
  adminPageController.error.accessDenied(req, res)
})

router.get('/404', (req, res) => {
  adminPageController.error.notFound(req, res)
})

router.get('/500', (req, res) => {
  adminPageController.error.internalError(req, res)
})

router.get('*', (req, res) => {
  adminPageController.error.notFound(req, res)
})

module.exports = router
