/**
 * The partials methods for routers
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router()

/**
 * Libs
 */
const moment = require('moment')
moment.locale('cs')
const osloveni = require('../libs/osloveni')
/**
 * Controllers
 */

module.exports.setLocalVariables = (req, res, next) => {
  res.locals = {
    currentPath: req.originalUrl,
    moment,
    osloveni
  }
  next()
}
router.all('*', this.setLocalVariables)

module.exports.onlyLoggedIn = (req, res, next) => {
  if (req.session.user === undefined) {
    if (req.originalUrl.includes('api')) {
      res.send('please-login')
    } else {
      res.redirect('/login')
    }
  } else {
    next()
  }
}

module.exports.onlyNonLoggedIn = (req, res, next) => {
  if (req.session.user === undefined) {
    next()
  } else {
    res.status(200).send('only-for-non-logged-in')
  }
}

module.exports.onlyAdmin = (req, res, next) => {
  if (req.session.user === undefined) {
    res.status(403).send('403')
  } else if (req.session.user.type === 'admin') {
    next()
  } else {
    res.status(403).send('403')
  }
}

/**
 * Export the router
 */
module.exports.router = router
