/**
 * The API router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router()

/**
 * Controllers
 */
const partials = require('./partials')
const resultController = require('../controllers/result')
const userController = require('../controllers/user')
const leagueController = require('../controllers/league')
const categoryController = require('../controllers/category')
const competitionController = require('../controllers/competition')
const districtController = require('../controllers/district')
const teamController = require('../controllers/team')

/**
 * Routes
 */

router.get('/', (req, res) => {
  res.send('Hello world!')
})

/**
 * Leagues
 */
router.post('/leagues/new', partials.onlyAdmin, (req, res) => {
  leagueController.new(req, res)
})

router.get('/leagues/list', (req, res) => {
  leagueController.list(req, res)
})

router.post('/leagues/delete', partials.onlyAdmin, (req, res) => {
  leagueController.delete(req, res)
})

router.post('/leagues/edit', partials.onlyAdmin, (req, res) => {
  leagueController.edit(req, res)
})

/**
 * Categories
 */
router.post('/categories/new', partials.onlyAdmin, (req, res) => {
  categoryController.new(req, res)
})

router.get('/categories/list', (req, res) => {
  categoryController.list(req, res)
})

router.post('/categories/delete', partials.onlyAdmin, (req, res) => {
  categoryController.delete(req, res)
})

router.post('/categories/edit', partials.onlyAdmin, (req, res) => {
  categoryController.edit(req, res)
})

/**
 * Competitions
 */
router.post('/competitions/new', partials.onlyAdmin, (req, res) => {
  competitionController.new(req, res)
})

router.get('/competitions/list', (req, res) => {
  competitionController.list(req, res)
})

router.get('/competitions/find-by-id/:id', (req, res) => {
  competitionController.findById(req, res)
})

router.post('/competitions/delete', partials.onlyAdmin, (req, res) => {
  competitionController.delete(req, res)
})

router.post('/competitions/edit', partials.onlyAdmin, (req, res) => {
  competitionController.edit(req, res)
})

/**
 * Districts
*/
router.post('/districts/new', partials.onlyAdmin, (req, res) => {
  districtController.new(req, res)
})

router.get('/districts/list', (req, res) => {
  districtController.list(req, res)
})

router.get('/districts/find-by-id/:id', (req, res) => {
  districtController.findById(req, res)
})

router.post('/districts/delete', partials.onlyAdmin, (req, res) => {
  districtController.delete(req, res)
})

router.post('/districts/edit', partials.onlyAdmin, (req, res) => {
  districtController.edit(req, res)
})

/**
 * Teams
*/
router.post('/teams/new', partials.onlyAdmin, (req, res) => {
  teamController.new(req, res)
})

router.get('/teams/list', (req, res) => {
  teamController.list(req, res)
})

router.get('/teams/find-by-name', (req, res) => {
  teamController.findByName(req, res)
})

router.get('/teams/find-by-id/:id', (req, res) => {
  teamController.findById(req, res)
})

router.post('/teams/delete', partials.onlyAdmin, (req, res) => {
  teamController.delete(req, res)
})

router.post('/teams/edit', partials.onlyAdmin, (req, res) => {
  teamController.edit(req, res)
})

/**
 * Results
 */
router.get('/results/list', (req, res) => {
  resultController.list(req, res)
})

router.get('/results/find-by-id/:id', (req, res) => {
  resultController.findById(req, res)
})

router.post('/results/new', partials.onlyLoggedIn, (req, res) => {
  resultController.new(req, res)
})

router.post('/results/edit', partials.onlyLoggedIn, (req, res) => {
  resultController.edit(req, res)
})

router.post('/results/delete', partials.onlyAdmin, (req, res) => {
  resultController.delete(req, res)
})

/**
 * Session
 */
router.get('/session', (req, res) => {
  res.send(req.session)
})

router.get('/session/destroy', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return console.error(err)
    }
    res.send('ok')
  })
})

/**
 * User's login, etc.
 */
router.post('/user/new', (req, res) => {
  userController.new(req, res)
})

router.post('/user/login', (req, res) => {
  userController.login(req, res)
})

router.post('/user/edit', partials.onlyLoggedIn, (req, res) => {
  userController.edit(req, res)
})

router.post('/user/forgot-password', (req, res) => {
  userController.enableRescue(req, res)
})

router.post('/user/set-new-password', (req, res) => {
  userController.setNewPassword(req, res)
})

router.get('/user/update-session', partials.onlyLoggedIn, (req, res) => {
  userController.updateSession(req, res)
})

router.post('/user/change-type', partials.onlyAdmin, (req, res) => {
  userController.changeType(req, res)
})

router.post('/user/delete', partials.onlyAdmin, (req, res) => {
  userController.delete(req, res)
})

router.get('/user/list', partials.onlyLoggedIn, (req, res) => {
  userController.list(req, res)
})

router.get('/user/logout', partials.onlyLoggedIn, (req, res) => {
  req.session.destroy()
  res.send('ok')
})

router.all('*', (req, res) => {
  return res.status(404).send('404')
})

module.exports = router
