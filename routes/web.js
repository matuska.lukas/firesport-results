/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Libs
 */

/**
 * Express router API
 */
const router = require('express').Router()

/**
 * Controllers
 */
const pageController = require('../controllers/page')
const leagueController = require('../controllers/league')
// const partials = require('./partials')

/**
 * Routes
 */

router.get('/', (req, res) => {
  pageController.homepage(req, res)
})

router.get('/leagues', (req, res) => {
  pageController.leagues(req, res)
})

router.get('/competitions/:leagueId', (req, res) => {
  leagueController.setLeagueSession(req, function (status) {
    if (status === 'err') {
      return this.error.internalError(req, res)
    }
    pageController.competitions(req, res)
  })
})

router.get('/results/:competitionId/:categoryId/', (req, res) => {
  pageController.results(req, res)
})

router.get('/contact', (req, res) => {
  pageController.contact(req, res)
})

/**
 * Login
 */

router.get('/login', (req, res) => {
  if (req.session.user === undefined) {
    return pageController.login(req, res)
  } else {
    return res.redirect('/')
  }
})

router.get('/register', (req, res) => {
  if (req.session.user === undefined) {
    return pageController.register(req, res)
  } else {
    return res.redirect('/')
  }
})

router.all('/logout', (req, res) => {
  req.session.user = undefined
  res.redirect('/login?status=logout-ok')
})

/**
 * Error pages
 */
router.get('/403', (req, res) => {
  pageController.error.accessDenied(req, res)
})

router.get('/404', (req, res) => {
  pageController.error.notFound(req, res)
})

router.get('/500', (req, res) => {
  pageController.error.internalError(req, res)
})

router.get('*', (req, res) => {
  pageController.error.notFound(req, res)
})

module.exports = router
